package dao;

import org.apache.ibatis.annotations.*;
import pojo.User;

import java.util.List;

public interface UserMapper {
    @Select("select * from user")
    List<User> getUsers();

    //方法如果有多个参数，每个参数前面都需加上@Param注解
    @Select("select * from user where id = #{id}")
    User getUserById(@Param("id") int id);

    //引用对象不需要写@Param
    @Insert("insert into user(id,name,pwd) value(#{id},#{name},#{password})")
    int addUser(User user);

    @Update("update user set name=#{name},pwd=#{password} where id = #{id}")
    int updateUser(User user);

    //如果有注解就要从跟着注解里面的走，否则会报错
    @Delete("delete from user where id = #{uid}")
    int deleteUser(@Param("uid") int id);

}
