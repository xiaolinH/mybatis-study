import dao.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import pojo.User;
import util.MybatisUtils;

public class UserMapperTest {

    @Test
    public void test(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        //底层主要应用反射
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        /*List<User> users = mapper.getUsers();
        for (User user : users) {
            System.out.println(user);
        }
        //查


        //增
        mapper.addUser(new User(4,"老四","123456"));

        //更新
        mapper.updateUser(new User(4,"hh","1234"));

        //删
        mapper.deleteUser(4);
        */
        User userById = mapper.getUserById(1);
        System.out.println(userById);

        sqlSession.close();
    }

}
