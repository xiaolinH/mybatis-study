package com.hxl.dao;

import com.hxl.pojo.User;
import com.hxl.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMapperTest {
    @Test
    public void test(){
        //获得SqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        try{
            //我们获得到对象，要拿到sql要么去dao拿要么去mapper中拿，但其实都一样
            //下面我们get到他的接口，返回接口类型
            //方式1：getMapper 执行sql
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            List<User> userList = userMapper.getUserList();
            //方式2：直接通过里面的select，根据返回值的来进行。不推荐使用
            //<User> userList = sqlSession.selectList("com.com.hxl.dao.UserDao.getUserList");
            for (User user : userList) {
                System.out.println(user);
            }
        }finally {
            //关闭SQLSession
            sqlSession.close();
        }
    }
    @Test
    public void getUserById(){
        //获得SqlSession对象
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        //我们获得到对象，要拿到sql要么去dao拿要么去mapper中拿，但其实都一样
        //下面我们get到他的接口，返回接口类型
        //getMapper 执行sql
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.getUserById(1);
        //方式2：直接通过里面的select，根据返回值的来进行。不推荐使用
        //<User> userList = sqlSession.selectList("com.com.hxl.dao.UserDao.getUserList");
        System.out.println(user);

        //关闭SQLSession
        sqlSession.close();

    }
    @Test
    public void getUserLike(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> userList = userMapper.getUserLike("%h%");
        for (User user : userList) {
            System.out.println(user);
        }
        sqlSession.close();

    }

    //增删改需要提交事务
    @Test
    public void addUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int r = userMapper.addUser(new User(4,"老四","123456"));
        if(r>0){
            System.out.println("插入成功！");
        }
        //提交事务。必须的，非常重要
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void addUser2(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("userId",5);
        map.put("userName","老五");
        map.put("userPassword","123456");
        mapper.addUser2(map);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void updateUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int r = userMapper.updateUser(new User(4,"老赖","123"));
        if(r>0){
            System.out.println("修改成功！");
        }
        //提交事务。必须的，非常重要
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void deleteUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        mapper.deleteUser(4);
        sqlSession.commit();
        sqlSession.close();
    }


}
