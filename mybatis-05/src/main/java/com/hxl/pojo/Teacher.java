package com.hxl.pojo;

import lombok.Data;

@Data
public class Teacher {
    private int id;
    private String name;
}
